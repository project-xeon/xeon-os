function loadCommands(){
    // Load command files
    system.filesystem.fs.readdirSync(`bin`).forEach(file=>{
        let path = `bin/${file}`;
        let data = system.filesystem.fs.readFileSync(path).toString();
        let load = new Function(data);
        load();
    });
}

function loadServices(resolve){
    // Load services
    system.eval(`(async ()=>{
        var dir = await drives.readdir('/etc/systemd/system');
        dir = dir.filter(el=>el.split('.')[1] == 'service');
        var stat = await drives.get();
        var res = [];
        for(var i = 0; i < dir.length; i++){
            var filename = dir[i];
            var path = '/etc/systemd/system/'+filename;
            if(stat[path] != null){
                res.push(stat[path]);
            }
        }
        return res;
    })()`, (files)=>{
        files.forEach(data=>{
            let load = new Function(`return ${data}`);
            let service = load();
            let cservice = system.systemd.get(service.name);
            if(cservice && cservice.enabled == false && cservice.started == false){
                system.systemd.set(service.name, new Service(service));
            } else if(!cservice){
                system.systemd.set(service.name, new Service(service));
            }
        });
        if(resolve){
            resolve();
        }
    });
}
loadCommands();