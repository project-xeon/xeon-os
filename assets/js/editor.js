const {ipcRenderer} = require('electron');

var path = null;

ipcRenderer.on('editor', (event, p, d)=>{
    path = p;
    editor.setValue(d);
});

function send(event, data){
    ipcRenderer.send('ws-send', event, data);
}

function save(cb){
    var data = editor.getValue();
    send('eval', `(async ()=>{
        try{
            await drives.write("${path}", ${JSON.stringify(Buffer.from(data).toJSON().data)});
            console.log('File saved');
        } catch(e) {
            console.log(e.message);
        }
    })()`);
}

window.onkeydown = function(ev){
    if(ev.ctrlKey && ev.keyCode == 83){
        save();
        alert("Saved");
    }
    if(ev.ctrlKey && ev.keyCode == 87){
        window.close();
    }
}