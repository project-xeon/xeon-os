function randomElement(array){
    let len = array.length-1;
    return array[Math.round(Math.random() * len)];
}
var colors = {};
colors.grayscale = [
    t=>{
        var reg = new RegExp(/¬\?/g);
        var match = reg.exec(t);
        while(match != null){
            t = t.replace(match[0], `¬${randomElement('wWRrGgBbyoPpVv*1234'.split(''))}`);
            match = reg.exec(t);
        }
        return t;
    },
    t=>{
        return t.replace(/¬w/g, '[[;#FFFFFF;]')
    },
    t=>{
        return t.replace(/¬W/g, '[[;#CBCBCB;]')
    },
    t=>{
        return t.replace(/¬R/g, '[[;#4C4C4C;]')
    },
    t=>{
        return t.replace(/¬r/g, '[[;#AAAAAA;]')
    },
    t=>{
        return t.replace(/¬G/g, '[[;#959595;]')
    },
    t=>{
        return t.replace(/¬g/g, '[[;#CDCDCD;]')
    },
    t=>{
        return t.replace(/¬B/g, '[[;#646464;]')
    },
    t=>{
        return t.replace(/¬b/g, '[[;#B2B2B2;]')
    },
    t=>{
        return t.replace(/¬y/g, '[[;#E1E1E1;]')
    },
    t=>{
        return t.replace(/¬o/g, '[[;#9B9B9B;]')
    },
    t=>{
        return t.replace(/¬P/g, '[[;#696969;]')
    },
    t=>{
        return t.replace(/¬p/g, '[[;#B8B8B8;]')
    },
    t=>{
        return t.replace(/¬V/g, '[[;#5A5A5A;]')
    },
    t=>{
        return t.replace(/¬v/g, '[[;#5E5E5E;]')
    },
    t=>{
        return t.replace(/¬\*/g, '[[;#ffffff;]') //#9bc1ff
    },
    t=>{
        return t.replace(/¬1/g, '[[;#A6A6A6;]')
    },
    t=>{
        return t.replace(/¬2/g, '[[;#A3A3A3;]')
    },
    t=>{
        return t.replace(/¬3/g, '[[;#868686;]')
    },
    t=>{
        return t.replace(/¬4/g, '[[;#E6E6E6;]')
    }
]

colors.rgb = [
    t=>{
        var reg = new RegExp(/¬\?/g);
        var match = reg.exec(t);
        while(match != null){
            t = t.replace(match[0], `¬${random('wWRrGgBbyoPpVv*1234'.split(''))}`);
            match = reg.exec(t);
        }
        return t;
    },
    t=>{
        return t.replace(/¬w/g, '[[;#FFFFFF;]')
    },
    t=>{
        return t.replace(/¬W/g, '[[;#CCCCCC;]')
    },
    t=>{
        return t.replace(/¬R/g, '[[;#FF0000;]')
    },
    t=>{
        return t.replace(/¬r/g, '[[;#FF8787;]')
    },
    t=>{
        return t.replace(/¬G/g, '[[;#00FF00;]')
    },
    t=>{
        return t.replace(/¬g/g, '[[;#87FF87;]')
    },
    t=>{
        return t.replace(/¬B/g, '[[;#2666FF;]')
    },
    t=>{
        return t.replace(/¬b/g, '[[;#00FFFF;]')
    },
    t=>{
        return t.replace(/¬y/g, '[[;#FFFF00;]')
    },
    t=>{
        return t.replace(/¬o/g, '[[;#FF8700;]')
    },
    t=>{
        return t.replace(/¬P/g, '[[;#FF00FF;]')
    },
    t=>{
        return t.replace(/¬p/g, '[[;#FF87FF;]')
    },
    t=>{
        return t.replace(/¬V/g, '[[;#8A29E0;]')
    },
    t=>{
        return t.replace(/¬v/g, '[[;#635294;]')
    },
    t=>{
        return t.replace(/¬\*/g, '[[;#ffffff;]') //#9bc1ff
    },
    t=>{
        return t.replace(/¬1/g, '[[;#2bff26;]')
    },
    t=>{
        return t.replace(/¬2/g, '[[;#2bd1f2;]')
    },
    t=>{
        return t.replace(/¬3/g, '[[;#bf52ff;]')
    },
    t=>{
        return t.replace(/¬4/g, '[[;#faff33;]')
    }
]

function color(text){
    //text = text.replace(/&not;/g, '¬');
    colors[window.localStorage.getItem('color_scheme')].forEach(color=>{
        text = color(text);
    });
    return `[[;#ffffff;]${text}]`;
}