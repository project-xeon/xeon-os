var ascii = `

                                    \`/ydNNm+       
                                 :hMMMh+.         
                               :dMMNo\`            
  \`\`                         -hMMMo\`              
/dsmMNho-                  \`sMMMh.                
/\`  :ymMMNs:              /NMMN+                  
       -yMMMNhs.        .hMMMd.                   
         \`+mMMMMy-     /NMMMo                     
            :yMMMMmo. yMMMN-                      
              \`+dMMMMMMMMd\`                       
    Project      -dMMMMMM:                             
       XEON       yMMMMMMm-                       
                 sMMMMyNMMMMy:                    
                yMMMm-  :yNMMMmo.                 
              \`dMMMh\`      :yMMMMd+\`              
             -mMMMs          \`/dMMMMh/\`           
            :NMMM/              .omMMMNy:         
           /MMMN-                  -sNMMMNy+\`     
          +MMMm.                      :yNMMMNs.   
         oMMMMsyh+\`                      -omMMMy. 
        /MMMMMm+\`                           -yMMMs
       .NMMNy-                                \`::\`
       :Md+\`                                      
`;

ascii = color(ascii);

var html = {
    encode: function(text){
        return $('<textarea></textarea>').text(text).html();
    },
    decode: function(text){
        return $('<textarea></textarea>').html(text).text();
    }
}

var term = $('#terminal').terminal((command)=>{
    command = command.trim();
    if(command.length > 0){
        term.echo(color(`${system.prompt}¬W${$.terminal.escape_brackets(`${html.encode(command).replace(/\¬/g, '&not;')}`)}`));
        return new Promise((resolve, reject)=>{
            system.events.commands.emit('command', command, (ret)=>{
                if(ret) print(ret);
                resolve();
            });
        });
    }
}, {
    echoCommand: false,
    greetings: ascii,
    pauseEvents: false,
    prompt: system.prompt,
    scrollOnEcho: true,
    scrollObject: $('#terminal')
});

function print(text){
    if(!text) return;
    switch(typeof text){
        case 'object':
            text = JSON.stringify(text, null, '   ');
        break;
        case 'function':
            text = '[function]';
        break;
    }
    term.echo(color($.terminal.escape_brackets(`${text}`)));
    $('#terminal').scrollTop($('#terminal')[0].scrollHeight);
}

system.terminal = {
    print: print
};