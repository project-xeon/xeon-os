({
    name: "ssh",
    start: function(sid){
        const service = this;
        let proc = service.StartProcess({
            interval: 10000,
            onStart: function(pid){
                service.Log(`Service started. [${pid}]`);
                let server = system.network.createServer({port: 22, timeout: 30000}, ()=>{
                    service.Log(`Server listening...`);
                });
                server.on('connect', socket=>{
                    service.Log('Socket connected.');
                    console.log(socket);
                    socket.on('data', data=>{
                        try {
                            console.log(data);
                            var dobj = JSON.parse(data.toString());
                            service.Log(`[${socket.uid}]: ${dobj.data}`);
                            system.events.commands.emit('ssh_command', dobj.data, (ret)=>{
                                socket.write(JSON.stringify({id: dobj.id, data: ret}));
                            });
                        } catch(e) {
                            console.log(e);
                            socket.write(JSON.stringify({id: dobj.id, data: e.message || e}));
                        }
                    });
                    socket.on('close', ()=>{
                        service.Log('Socket disconnected.');
                    });
                });
                this.on('kill', ()=>{
                    server.Close();
                });
            },
            onKill: function(sig){
                service.Log(`Service exited with code ${sig}.`);
            }
        });
    },
    stop: function(){
        const service = this;
    }
})