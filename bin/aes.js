system.commands.set('aes', (args, resolve)=>{
    args = system.parseArguments(args);
    switch(args[0]){
        case 'encrypt':
            var k = args[1];
            var d = args[2];
            system.eval(`(async ()=>{
                return await crypto.aes.encrypt("${k}", "${d}");
            })()`, res=>{
                resolve(res);
            });
        break;
        case 'decrypt':
            var k = args[1];
            var d = args[2];
            system.eval(`(async ()=>{
                return await crypto.aes.decrypt("${k}", "${d}");
            })()`, res=>{
                resolve(res);
            });
        break;
    }
});