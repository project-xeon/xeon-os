system.commands.set('cash', (args, resolve)=>{
    args = system.parseArguments(args);
    switch(args[0]){
        case 'secure_balance':
            system.eval(`(async ()=>{
                return await cash.secure_balance("${args[1]}");
            })()`, (res)=>{
                if(typeof res == 'number')
                    resolve(`Secure Balance: $${res}`);
                else resolve(res);
            });
        break;
        case 'balance':
            system.eval(`(()=>{
                return cash.balance;
            })()`, (res)=>{
                if(typeof res == 'number')
                    resolve(`Balance: $${res}`);
                else resolve(res);
            });
        break;
        default:
            resolve('Invalid subcommand.');
        break;
    }
});