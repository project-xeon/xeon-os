system.commands.set('cat', (args, resolve)=>{
    let path = system.resolve(args.join(' '));
    system.eval(`(async ()=>{
        try {
            let d = await drives.read("${path}");
            return d.toString();
        } catch(e) {
            return e.message;
        }
    })()`, (ret)=>{
        resolve(ret);
    });
});