system.commands.set('cd', (args, resolve)=>{
    let path = system.resolve(args.join(' '));
    system.eval(`(async ()=>{
        let exists = await drives.exists("${path}");
        if(exists){
            let stat = await drives.get();
            if(stat["${path}"] == null) return true;
            else return false;
        } else return false;
    })()`, (ret)=>{
        if(ret == true){
            system.cwd = path;
            term.set_prompt(system.prompt);
            resolve();
        } else {
            resolve('¬RInvalid directory.');
        }
    });
});