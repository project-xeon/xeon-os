system.commands.set('colors', (args, resolve)=>{
    if(args.length == 0){
        resolve();
        return;
    }
    if(args[0].toLowerCase()[0] == 'g'){
        window.localStorage.setItem('color_scheme', 'grayscale');
        window.location.reload();
    } else if(args[0].toLowerCase()[0] == 'r'){
        window.localStorage.setItem('color_scheme', 'rgb');
        window.location.reload();
    }
});