system.commands.set('connect', (args, resolve)=>{
    system.eval(`connect ${args.join(' ')}`, (res)=>{
        if(res.success){
            system.network.address = res.address;
            system.cwd = '/';
            term.set_prompt(system.prompt);
            resolve(res.msg);
        } else {
            resolve(res.error);
        }
    });
});