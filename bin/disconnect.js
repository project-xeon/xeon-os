system.commands.set('disconnect', (args, resolve)=>{
    system.eval(`disconnect`, (res)=>{
        if(res.success){
            system.network.address = res.address;
            system.cwd = '/';
            term.set_prompt(system.prompt);
            resolve(res.msg);
        } else {
            resolve(res.error);
        }
    });
});