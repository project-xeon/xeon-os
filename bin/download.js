system.commands.set('download', (args, resolve)=>{
    var path = system.resolve(args.join(' '));
    system.eval(`(async ()=>{
        try {
            let d = await drives.read("${path}");
            return {success: true, buffer: d.toJSON()};
        } catch(e){
            return e.message;
        }
    })()`, (res)=>{
        if(res.success && res.success == true){
            res.buffer = system.createBuffer(res.buffer);
            system.filesystem.dialog.showSaveDialog((filename)=>{
                if(filename == undefined){
                    resolve('File not saved.');
                } else {
                    system.filesystem.fs.writeFile(filename, res.buffer, (err)=>{
                        if(err) {
                            resolve(`Error saving file: ${err.message}`);
                        } else {
                            resolve(`File saved to: ${filename}`);
                        }
                    });
                }
            });
        } else {
            resolve();
        }
    });
});