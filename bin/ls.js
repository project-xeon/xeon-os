system.commands.set('ls', (args, resolve)=>{
    let path = system.resolve(args.join(' '));
    system.eval(`(async ()=>{
        let dir = await drives.readdir("${path}");
        let drive = await drives.get();
        let data = [];
        let path = "${path}";
        dir.forEach(d=>{
            let p = path+d;
            data.push({filename: d, directory: (drive[p] == null)});
        });
        return data;
    })()`, (files)=>{
        if(typeof files == 'string'){
            try {
                files = JSON.parse(files);
            } catch(e) {
                resolve(e.message);
            }
        }
        if(typeof files == 'object' && files != null) {
            resolve(files.map(file=>{
                return file.directory ? `¬B${file.filename}` : `¬g${file.filename}`;
            }).join('  '));
        } else {
            resolve('derp');
        }
    });
});