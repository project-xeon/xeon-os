system.commands.set('mkdir', (args, resolve)=>{
    let rec = args.findIndex(el=>{
        return el == '-p' || el == '--parents';
    });
    if(rec != -1){
        args.splice(rec, 1);
        rec = 'true';
    } else rec = 'false';
    let path = system.resolve(args.join(' '));
    system.eval(`(async ()=>{
        return await drives.mkdir("${path}", ${rec});
    })()`, (ret)=>{
        if(ret == true){
            resolve(`Created directory at ${path}`);
        } else {
            resolve(ret);
        }
    });
});