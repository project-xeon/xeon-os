system.commands.set('nano', (args, resolve)=>{
    if(system.editor == null) {
        var path = system.resolve(args.join(' '));
        system.eval(`(async ()=>{
            try {
                let exists = await drives.exists("${path}");
                if(exists){
                    let data = await drives.read("${path}");
                    return {success: true, data: data.toString()};
                } else {
                    return {success: true, data: ""};
                }
            } catch(e) {
                return {success: false, error: e.message};
            }
        })()`, (ret)=>{
            if(ret.success){
                system.openEditor(path, ret.data, resolve);
            } else {
                print(`¬R${ret.error}`);
                resolve();
            }
        });
    } else {
        print('¬REditor already open.');
        resolve();
    }
});