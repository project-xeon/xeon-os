system.commands.set('request', (args, resolve)=>{
    var reg = new RegExp(/(?:http:\/\/|https:\/\/|)([\w-_\.]*)\/?(.*)$/);
    var path = args[0];
    var method = args[1].toUpperCase();
    var body = args.slice(2);
    if(!body) body = "";
    else body = body.join(' ');
    if(method != 'GET' && method != 'POST' && method != 'PUT' && method != 'DELETE') {
        resolve('Invalid method.');
    } else {
        var m = reg.exec(path);
        if(m == null){
            resolve('Bad url');
        } else {
            var host = m[1];
            var uri = `/${m[2]}`;
            console.log(m);
            var id = system.uuid();
            system.network.http.set(id, (res)=>{
                res.body = system.createBuffer(res.body);
                resolve(res);
            });
            system.eval(`(async ()=>{
                net.send("${host}", 80, JSON.stringify({id: "${id}", request: {hostname: "${host}", method: "${method}", path: "${uri}", body: JSON.stringify('${body}')}}));
            })()`);
        }
    }
});