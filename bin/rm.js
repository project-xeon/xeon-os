system.commands.set('rm', (args, resolve)=>{
    var i = args.findIndex(el=>(el == '-r' || el == '--recursive'));
    if(i != -1) {
        args.splice(i, 1);
        i = true;
    } else {
        i = false;
    }
    var path = system.resolve(args.join(' '));
    system.eval(`(async ()=>{
        await drives.rm("${path}", ${i});
    })()`, ret=>{
        if(ret != true){
            resolve(ret);
        } else resolve();
    });
});