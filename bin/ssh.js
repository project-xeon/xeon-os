system.commands.set('ssh', (args, resolve)=>{
    let socket = system.network.createClient({host: args[0], port: 22, timeout: 5000}, ()=>{
        system.cwd = '/';
        let ssh = new SSHCon(socket);
        system.network.ssh = ssh;
        ssh.on('close', ()=>{
            system.network.ssh = null;
            term.set_prompt(system.prompt);
            system.terminal.print('SSH Connection Destroyed.');
        });
        term.set_prompt(system.prompt);
        resolve('SSH Connection Created.');
    });
    socket.on('error', (e)=>{
        resolve(e.message || e);
    });
});