system.commands.set('systemctl', (args, resolve)=>{
    switch(args[0]){
        case 'start':
            var name = args[1];
            if(system.systemd.get(name)){
                let service = system.systemd.get(name);
                if(service.started == false){
                    service.stdout = "";
                    service.Start();
                    resolve();
                } else {
                    resolve('Service is already running.');
                }
            } else {
                resolve('Service not found.');
            }
        break;
        case 'restart':
            var name = args[1];
            if(system.systemd.get(name)){
                let service = system.systemd.get(name);
                if(service.started == true){
                    service.Stop();
                    var s = service.service;
                    var sout = service.stdout;
                    system.systemd.set(s.name, new Service(s));
                    system.systemd.get(s.name).stdout = sout;
                    service.stdout = "";
                    service.Start();
                    resolve();
                } else {
                    service.stdout = "";
                    service.Start();
                    resolve();
                }
            } else {
                resolve('Service not found.');
            }
        break;
        case 'stop':
            var name = args[1];
            if(system.systemd.get(name)){
                let service = system.systemd.get(name);
                if(service.started == true){
                    service.Stop();
                    var s = service.service;
                    var sout = service.stdout;
                    system.systemd.set(s.name, new Service(s));
                    system.systemd.get(s.name).stdout = sout;
                    resolve();
                } else {
                    resolve('Service is not running.');
                }
            } else {
                resolve('Service not found.');
            }
        break;
        case 'enable':
            var name = args[1];
            if(system.systemd.get(name)){
                let service = system.systemd.get(name);
                if(service.enabled == false){
                    service.Enable();
                    resolve();
                } else {
                    resolve('Service is already enabled.');
                }
            } else {
                resolve('Service not found.');
            }
        break;
        case 'disable':
            var name = args[1];
            if(system.systemd.get(name)){
                let service = system.systemd.get(name);
                if(service.enabled == true){
                    service.Disable();
                    resolve();
                } else {
                    resolve('Service is not enabled.');
                }
            } else {
                resolve('Service not found.');
            }
        break;
        case 'status':
            var name = args[1];
            if(system.systemd.get(name)){
                let service = system.systemd.get(name);
                resolve(service.stdout);
            } else {
                resolve('Service not found.');
            }
        break;
        case 'daemon-reload':
            loadServices();
            resolve();
        break;
    }
});