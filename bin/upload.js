system.commands.set('upload', (args, resolve)=>{
    var path = system.resolve(args.join(' '));
    system.filesystem.dialog.showOpenDialog((fileNames)=>{
        if(fileNames == undefined){
            resolve('No file selected.');
        } else {
            var filepath = fileNames[0];
            var data = system.filesystem.fs.readFileSync(filepath);
            system.eval(`(async ()=>{
                try {
                    await drives.write("${path}", ${JSON.stringify(data.toJSON().data)});
                    return true;
                } catch(e){
                    return e.message;
                }
            })()`, (res)=>{
                if(res == true){
                    resolve('Upload complete.');
                } else {
                    resolve(res);
                }
            });
        }
    });
});