system.commands.set('web', (args, resolve)=>{
    var reg = new RegExp(/(?:http:\/\/|https:\/\/|)([\w-_\.]*)\/?(.*)$/);
    var path = args[0];
    var m = reg.exec(path);
    if(m == null){
        resolve('Bad url');
    } else {
        var host = m[1];
        var uri = `/${m[2]}`;
        var id = system.uuid();
        system.network.http.set(id, (res)=>{
            try {
                res.body = system.createBuffer(res.body);
                if(res.headers['Content-type'].split('/')[0] == 'text'){
                    res.body = res.body.toString();
                    resolve(res.body);
                } else {
                    var filed = res.body;
                    res.body = 'Binary data';
                    system.filesystem.dialog.showSaveDialog((filename)=>{
                        system.filesystem.fs.writeFile(filename, filed, (err)=>{
                            if(err) resolve(err.message);
                            else {
                                resolve(`File saved to ${filename}`);
                            }
                        });
                    });
                }
            } catch(e) {
                console.log(e);
                resolve(res.body);
            }
        });
        system.eval(`(async ()=>{
            net.send("${host}", 80, JSON.stringify({id: "${id}", request: {hostname: "${host}", method: "GET", path: "${uri}", body: ""}}));
        })()`);
    }
});