system.commands.set('xpm', (args, resolve)=>{
    args = system.parseArguments(args);
    switch(args[0]){
        case 'install':
            args = args.slice(1);
            var name = args[0];
            var version = 'latest';
            if(name.split('@').length > 1){
                name = args[0].split('@')[0];
                version = args[0].split('@')[1];
            }
            system.network.request.get({
                uri: `http://pkg.xeon.net/install?name=${name}&version=${version}`,
                json: true
            }).then(([res, body])=>{
                if(res.code == 404){
                    resolve('Package not found.');
                } else {
                    system.filesystem.decompress(name, version, body).then((res)=>{
                        var msg = `${name}@${version} installed successfully.`
                        if(res.hooks.length > 0){
                            var er = 'Unknown error occurred during installation.';
                            if(res.hooks.some(file=>{
                                var d = system.filesystem.fs.readFileSync(file).toString();
                                var l = new Function(`return ${d}`);
                                try {
                                    if(!l()){
                                        return true;
                                    }
                                } catch(e) {
                                    er = e.message;
                                    return true;
                                }
                            })){
                                res.failed();
                                resolve(`xpm - [${name} ${version}] ¬RERROR: ¬r${er}`);
                            } else {
                                resolve(msg);
                            }
                        } else {
                            resolve(msg)
                        }
                    }, (e)=>{
                        resolve(e.message || e);
                    });
                }
            }, e=>{
                resolve(e.message || e);
            });
        break;
        case 'publish':
            args = args.slice(1);
            system.filesystem.compress(args[0]).then((obj)=>{        
                var frame = 0;
                var cl = false;
                term.echo('Publishing package [[;#87FF87;]⠓]');
                var u = setInterval(()=>{
                    var frames = [
                        "⠋",
                        "⠙",
                        "⠹",
                        "⠸",
                        "⠼",
                        "⠴",
                        "⠦",
                        "⠧",
                        "⠇",
                        "⠏"
                    ];
                    if(!cl) {
                        term.update(-1, `Publishing package [[;#87FF87;]${frames[frame]}]`);
                        ++frame;
                        if(frame > frames.length-1) frame = 0;
                    }
                }, 80);
                system.network.request.post({
                    uri: 'http://pkg.xeon.net/publish',
                    json: true,
                    body: {
                        package: {
                            name: obj.name,
                            version: obj.version,
                            tar: obj.data,
                            hash: obj.hash
                        }
                    }
                }).then(([res, body])=>{
                    cl = true;
                    clearInterval(u);
                    if(body.success){
                        resolve('Package published successfully.');
                    } else {
                        resolve(`Error: ${body.error}`);
                    }
                }, (e)=>{
                    resolve(e.message);
                });
            }, (e)=>{
                resolve(e.message || e);
            });
        break;
        default:
            resolve(`Unknown subcommand ${args[0]}`);
        break;
    }
});